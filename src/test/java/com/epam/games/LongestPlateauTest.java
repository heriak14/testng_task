package com.epam.games;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.Mockito;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;


public class LongestPlateauTest {
    private static final Logger LOG = LogManager.getLogger();
    private static LongestPlateau plateau;
    private static Findable findable;

    @DataProvider
    public static Object[][] getTestData() {
        return new Object[][]{
                {new int[]{1, 2, 2, 0, 4, 5, 5, 5, 5, 4, 7}, new int[]{5, 9, 4}},
                {new int[]{}, new int[]{0, 0, 0}},
                {new int[]{1, 2, 2, 3, 1}, new int[] {3, 4, 1}},
                {null, null}
        };
    }

    @BeforeClass
    static void initTestArray() {
        LOG.info("Inject mock into Findable");
        findable = Mockito.mock(Findable.class);
        plateau = new LongestPlateau(findable);
    }

    @Test(dataProvider = "getTestData")
    void testFindByFindable(int[] testArray, int[] result) {
        LOG.info("Test finding longest plateau by Findable interface\n");
        LOG.info("Initialize expected result: ");
        LOG.info(Arrays.toString(result) + "\n");
        LOG.info("Set expected behavior to mocked object\n");
        when(findable.find(testArray)).thenReturn(result);
        assertEquals(result, plateau.findViaFindable(testArray));
    }

    @Test(dataProvider = "getTestData")
    void testFindPlateau(int[] testArray, int[] result) {
        LOG.info("Test finding longest plateau by written method");
        assertEquals(plateau.findPlateau(testArray), result);
    }
}