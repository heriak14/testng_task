package com.epam.games;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class MinesweeperTest {
    private static final Logger LOG = LogManager.getLogger();
    private static boolean[][] testField;
    private static int[][] result;
    private Minesweeper minesweeper;

    @BeforeClass
    static void initData() {
        LOG.info("Initialize test and expected data\n");
        testField = new boolean[][]{
                {false, true, true, false},
                {true, true, true, true},
                {false, true, false, true},
                {false, true, true, true}};
        result = new int[][]{
                {3, 1, 1, 3},
                {1, 1, 1, 1},
                {4, 1, 8, 1},
                {2, 1, 1, 1}};
    }

    @Test(invocationCount = 5)
    public void testFillField() {
        LOG.info("Test filling start matrix with probability 0.75\n");
        Reporter.log("Test filling start matrix with probability 0.75\n");
        minesweeper = new Minesweeper(4, 4, 0.75);
        assertNotNull(minesweeper.getField());
    }

    @Test
    public void testGetSolution() {
        LOG.info("Test getSolution method\n");
        minesweeper = new Minesweeper(testField);
        assertEquals(minesweeper.getSolution(), result);
    }

    @Test
    public void testCountNeighbourMines() {
        minesweeper = new Minesweeper(testField);
        LOG.info("Count mines at position (0, 0)\n");
        assertSame(minesweeper.countNeighboursMines(0, 0), 3);
        LOG.info("Count mines at position (0, 3)\n");
        assertSame(minesweeper.countNeighboursMines(0, 3), 3);
        LOG.info("Count mines at position (2, 2)\n");
        assertSame(minesweeper.countNeighboursMines(2, 2), 8);
    }

    @Test
    public void testGetInt() {
        Minesweeper ms = new Minesweeper(5, 5, 0.75);
        LOG.info("Check parsing 'true' to int\n");
        assertEquals(ms.getInt(true), 1);
        LOG.info("Check parsing 'false' to int\n");
        assertEquals(ms.getInt(false), 0);
    }
}
