package com.epam.games;

import java.util.Objects;

public class LongestPlateau {
    private int start;
    private int end;
    private int length;
    private Findable findable;

    public LongestPlateau(Findable findable) {
        this.findable = findable;
    }

    public int getLength() {
        return length;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    int[] findPlateau(int[] array) {
        if (Objects.isNull(array)) {
            return null;
        }
        int length = 0;
        int start = 0;
        int end = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                int tmp_length = 1;
                for (int j = i + 1; j < array.length - 1; j++) {
                    if (array[j] != array[j + 1]) {
                        if (array[j] > array[j + 1] && tmp_length > length) {
                            length = tmp_length;
                            start = i + 1;
                            end = j + 1;
                        }
                        //i = j;
                        break;
                    } else {
                        tmp_length++;
                    }
                }
            }
        }
        return new int[] {start, end, length};
    }

    public void solve(int[] array) {
        int[] res = findPlateau(array);
        start = res[0];
        end = res[1];
        length = res[2];
    }

    int[] findViaFindable(int[] array) {
        return findable.find(array);
    }
}
