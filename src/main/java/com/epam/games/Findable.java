package com.epam.games;

public interface Findable {
    int[] find(int[] array);
}
