package com.epam.games;

import java.util.Random;

public class Minesweeper {
    private static final Random RANDOM = new Random();
    private static final int HUNDRED_PERCENT = 100;
    private boolean[][] field;

    public Minesweeper(int m, int n, double probability) {
        fillField(m, n, probability);
    }

    public Minesweeper(boolean[][] field) {
        this.field = field;
    }

    boolean[][] getField() {
        return field;
    }

    public int[][] getSolution() {
        int[][] solution = new int[field.length][field[0].length];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                solution[i][j] = field[i][j] ? getInt(field[i][j]) : countNeighboursMines(i, j);
            }
        }
        return solution;
    }

    private void fillField(int m, int n, double probability) {
        field = new boolean[m][n];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = RANDOM.nextInt(HUNDRED_PERCENT) < probability * HUNDRED_PERCENT;
            }
        }
    }

    int countNeighboursMines(int i, int j) {
        int mines = 0;
        if (i > 0) {
            mines += getInt(field[i - 1][j]);
            if (j > 0) {
                mines += getInt(field[i - 1][j - 1]);
            }
            if (j < field[i].length - 1) {
                mines += getInt(field[i - 1][j + 1]);
            }
        }
        if (j > 0) {
            mines += getInt(field[i][j - 1]);
        }
        if (j < field[i].length - 1) {
            mines += getInt(field[i][j + 1]);
        }
        if (i < field.length - 1) {
            mines += getInt(field[i + 1][j]);
            if (j > 0) {
                mines += getInt(field[i + 1][j - 1]);
            }
            if (j < field[i].length - 1) {
                mines += getInt(field[i + 1][j + 1]);
            }
        }
        return mines;
    }

    int getInt(boolean b) {
        return b ? 1 : 0;
    }
}
